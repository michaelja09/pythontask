from subprocess import Popen
import time
import os
from subprocess import Popen



commands = ['sleep 3','ls -l /','find /','sleep 4','find /usr','date','sleep 5','uptime']


times = []
total_time = 0
ps = []
for command in commands:
	start = time.time()
	p = Popen(command, shell=True)
	ru = os.wait4(p.pid, 0)[2]
	ps.append(p.poll())
	elapsed = time.time() - start
	print("Time elapsed {:.3f}".format(elapsed))
	times.append(elapsed)
	total_time += elapsed

while True:
  ps_status = [p.poll() for p in ps]
  if all([x is not None for x in ps_status]):
    break


print("Total time taken is", total_time)
print("Average time taken is", sum(times)/len(times))
print("Maximum time taken is",max(times)," by ",commands.index(times.index(max(times))))
print("Minimum time taken is",min(times)," by ",commands.index(times.index(min(times))))
